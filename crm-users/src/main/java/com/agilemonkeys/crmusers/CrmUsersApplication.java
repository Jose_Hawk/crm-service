package com.agilemonkeys.crmusers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrmUsersApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrmUsersApplication.class, args);
    }

}
