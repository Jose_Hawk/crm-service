package com.hawcode.crmcustomers.services;

import com.hawcode.crmcustomers.models.Customer;
import com.hawcode.crmcustomers.repositories.CustomerRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public Customer createCustomer(Customer customerInformation) {
        Customer customer = new Customer();
        customer.setName(customerInformation.getName());
        customer.setSurname(customerInformation.getSurname());
        customer.setInsertTime(LocalDateTime.now());

        customerRepository.save(customer);

        return customer;
    }

    public Customer getCustomerById(UUID customerId) throws NotFoundException {
        Optional<Customer> optionalCustomer = customerRepository.findById(customerId);

        if (optionalCustomer.isPresent()) {
            return optionalCustomer.get();
        } else {
            throw new NotFoundException("Customer not found");
        }
    }

    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    public Customer updateCustomer(UUID customerId, Customer newCustomerInformation) throws NotFoundException {
        Customer customer = getCustomerById(customerId);

        updateCustomerInformation(customer, newCustomerInformation);

        return customer;
    }

    public void deleteCustomer(UUID customerId) throws NotFoundException {
        Customer customer = getCustomerById(customerId);

        customerRepository.delete(customer);
    }

    private void updateCustomerInformation(Customer oldCustomer, Customer newCustomer) {
        oldCustomer.setName(newCustomer.getName());
        oldCustomer.setSurname(newCustomer.getSurname());
        oldCustomer.setModificationTime(LocalDateTime.now());
    }
}