package com.hawcode.crmcustomers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrmCustomersApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrmCustomersApplication.class, args);
    }

}
