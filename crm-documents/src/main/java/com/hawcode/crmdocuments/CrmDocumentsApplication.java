package com.hawcode.crmdocuments;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrmDocumentsApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrmDocumentsApplication.class, args);
    }

}
